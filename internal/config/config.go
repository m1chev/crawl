// @Title
// @Description
// @Author
// @Update
package config

type Config struct {
	COLLY    *collyConfig
	URL      *urlConfig
	SELECTOR *selectorConfig
}

type collyConfig struct {
	AllowedDomains string `envconfig:"COLLY_ALLOWED_DOMAINS" default:"www.imot.bg"`
	// COLLY_CACHE_DIR (string)
	// COLLY_DETECT_CHARSET (y/n)
	// COLLY_DISABLE_COOKIES (y/n)
	// COLLY_DISALLOWED_DOMAINS (comma separated list of domains)
	// COLLY_IGNORE_ROBOTSTXT (y/n)
	// COLLY_FOLLOW_REDIRECTS (y/n)
	// COLLY_MAX_BODY_SIZE (int)
	// COLLY_MAX_DEPTH (int - 0 means infinite)
	// COLLY_PARSE_HTTP_ERROR_RESPONSE (y/n)
	// COLLY_USER_AGENT (string)
}

type urlConfig struct {
	VistiSite    string `envconfig:"URL_VISTI" default:"https://www.imot.bg/pcgi/imot.cgi?act=3&slink=7xvhqv&f1="`
	LastPage     int    `envconfig:"URL_LAST_PAGE" default:"2"`
	SearchString string `envconfig:"URL_SEARCH_STRING" default:"act=5"`
}

type selectorConfig struct {
	ElementLinks string `envconfig:"SELECTOR_ELEMENT_LINKS" default:"a.lnk1[href]"`
	Link         string `envconfig:"SELECTOR_LINK" default:"href"`
	InfoContent  string `envconfig:"SELECTOR_INFO_CONTENT" default:"table"`
	Description  string `envconfig:"SELECTOR_DESCRIPTION" default:"#description_div"`
}
