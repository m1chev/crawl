// @ToDo:  attache debugger to collector;
// @Title: Scrawler
// @Description: Exports property info in Sofia Center from imot.bg
// @Author M1chev
// @Update
package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/url"
	"strconv"
	"strings"

	"github.com/gocolly/colly/v2"
	"github.com/kelseyhightower/envconfig"
	"golang.org/x/text/encoding/charmap"
	"golang.org/x/text/transform"

	"gitlab.com/m1chev/crawl/internal/config"
)

// Imot - structure with the needed fields
type Imot struct {
	Link string `json:"link"`
	Info string `json:"info"`
}

func main() {
	// load configuration from environment
	var cfg config.Config
	if err := envconfig.Process("", &cfg); err != nil {
		log.Fatalf("loading configuration failed: %v\n", err)
	}

	// Instantiate default collector
	c := colly.NewCollector(
		colly.AllowedDomains(cfg.COLLY.AllowedDomains),
	)

	// Create another collector to scrape details
	detailCollector := c.Clone()
	imots := make([]Imot, 0, 200)

	c.OnHTML(cfg.SELECTOR.ElementLinks, func(e *colly.HTMLElement) {
		e.Request.Visit(e.Attr(cfg.SELECTOR.Link))
	})

	// Before making a request print "Visiting ..."
	c.OnRequest(func(r *colly.Request) {
		log.Println("Visiting:", r.URL.String())
	})

	// On every a HTML element which has name attribute call callback
	c.OnHTML(`a`, func(e *colly.HTMLElement) {
		// Activate detailCollector if the link contains "act=5"
		imotURL := e.Request.AbsoluteURL(e.Attr(cfg.SELECTOR.Link))
		if strings.Index(imotURL, cfg.URL.SearchString) != -1 {
			detailCollector.Visit(imotURL)
		}
	})

	var visited url.URL
	detailCollector.OnHTML(cfg.SELECTOR.InfoContent, func(e *colly.HTMLElement) {
		if visited != *e.Request.URL {
			log.Println("Imot found:", e.Request.URL)
			rInUTF8 := transform.NewReader(strings.NewReader(e.ChildText(cfg.SELECTOR.Description)), charmap.Windows1251.NewDecoder())
			decBytes, _ := ioutil.ReadAll(rInUTF8)
			info := string(decBytes)

			imot := Imot{
				Link: e.Request.URL.String(),
				Info: info,
			}
			imots = append(imots, imot)
		}
		visited = *e.Request.URL
	})

	for i := 1; i < cfg.URL.LastPage; i++ {
		c.Visit(cfg.URL.VistiSite + strconv.Itoa(i))
	}

	file, _ := JSONMarshal(imots)
	err := ioutil.WriteFile("./output/imots.json", file, 0644)
	if err != nil {
		log.Println("ioutil.WriteFile failed:", err)
	}
}

// JSONMarshal - marshal json while escaping html symbols
func JSONMarshal(t interface{}) ([]byte, error) {
	buffer := &bytes.Buffer{}
	enc := json.NewEncoder(buffer)
	enc.SetEscapeHTML(false)
	enc.SetIndent("", " ")
	err := enc.Encode(t)
	return buffer.Bytes(), err
}
