module gitlab.com/m1chev/crawl

go 1.16

require (
	github.com/gocolly/colly/v2 v2.1.0
	github.com/kelseyhightower/envconfig v1.4.0
	golang.org/x/text v0.3.2
)
